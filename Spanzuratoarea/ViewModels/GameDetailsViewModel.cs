﻿using Spanzuratoarea.Models;
using Spanzuratoarea.Pictures;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;
using System.Windows.Controls;
using Spanzuratoarea.Views;
using System.Windows.Input;

namespace Spanzuratoarea.ViewModels
{
    class GameDetailsViewModel
    {
        public ObservableCollection<GameDetails> gameDetails { get; set; }
        string filePath = @"..\..\..\Files\GameDetails.txt";
        List<string> wordsFromFile = new List<string>();
        string randomWord="";

        public GameDetailsViewModel()
        { 
            gameDetails = new ObservableCollection<GameDetails> { };
            string phrase = File.ReadAllLines(filePath).Last();
            string[] words = phrase.Split('\n');
            foreach (string word in words)
            {
                string[] items = word.Split(' ');
                if (word != "\r")
                    if (items[0] != "" && items[1] != "")
                        gameDetails.Add(new GameDetails() { PlayersName = items[0], PlayersPath = items[1] });
            }   
        }

        public string GetRandomWord(string checkedCategory)
        {
            string categoryPath = @"..\..\..\Files\Words\" + checkedCategory + ".txt";
            string phrase = File.ReadAllText(categoryPath);
            string[] words = phrase.Split('\n');
            wordsFromFile.Clear();
            foreach (string word in words)
            {
                string[] items = word.Split('\r');
                wordsFromFile.Add(items[0]);
            }
            Random rnd = new Random(); 
            int randomWordIndex = rnd.Next(1, wordsFromFile.Count());
            randomWord = wordsFromFile[randomWordIndex - 1];
            return randomWord;
        }


        

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
