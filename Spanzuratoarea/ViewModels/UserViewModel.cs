﻿using Spanzuratoarea.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Spanzuratoarea.Pictures
{

    class UserViewModel
    {
        public ObservableCollection<User> Users { get; set; }
        public ObservableCollection<Statistics> PlayersStatistics { get; set; }
        string filePath = @"..\..\..\Files\Players.txt";
        string statisticsPath = @"..\..\..\Files\Statistics.txt";

        public UserViewModel()
        {
            Users = new ObservableCollection<User> { };
            PlayersStatistics = new ObservableCollection<Statistics> { };
            string phrase = File.ReadAllText(filePath);
            string[] words = phrase.Split('\n');
            foreach (string word in words)
            {

                string[] items = word.Split(' ');
                if (word != "\r")
                    if (items[0] != "" && items[1] != "")
                    {
                        Users.Add(new User() { Name = items[0], Path = items[1] });
                    }
            }

            
            string phrase2 = File.ReadAllText(statisticsPath);
            string[] words2 = phrase2.Split('\n');
            foreach (string word in words2)
            {

                string[] item = word.Split('\r');
                string[] items = item[0].Split(' ');
                if (word != "\r")
                    if (items[0] != "" && items[1] != "" && items[2]!="")
                    {
                        PlayersStatistics.Add(new Statistics { Name = items[0], Winnings = items[1], Loss = items[2] });
                    }
            }
        }

        public void AddPlayer(string playerName, string playerPath)
        {
            Users.Add(new User() { Name = playerName, Path = playerPath });
            PlayersStatistics.Add(new Statistics { Name = playerName, Winnings = "0", Loss = "0" });
            string addToTxt = playerName + " " + playerPath;
            List<string> da = new List<string>();
            da.Add(addToTxt);
            File.AppendAllLines(filePath, da);

            string addtoStatistics = playerName + " 0 0";
            List<string> addTotxt = new List<string>();
            addTotxt.Add(addtoStatistics);
            File.AppendAllLines(statisticsPath, addTotxt);
  
        }
    }
}
