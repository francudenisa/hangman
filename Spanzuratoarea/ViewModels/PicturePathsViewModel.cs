﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spanzuratoarea.Models;


namespace Spanzuratoarea.ViewModels
{
    class PicturePathsViewModel
    {
        public ObservableCollection<PicturePaths> PicturesPath { get; set; }

        public PicturePathsViewModel()
        {
            PicturesPath = new ObservableCollection<PicturePaths> 
            {
                new PicturePaths{Path = "../../Pictures/pic1.jpg"},
                new PicturePaths{Path = "../../Pictures/pic2.jpg"},
                new PicturePaths{Path = "../../Pictures/pic3.jpg"},
                new PicturePaths{Path = "../../Pictures/pic4.jpg"},
                new PicturePaths{Path = "../../Pictures/pic5.jpg"},
                new PicturePaths{Path = "../../Pictures/pic6.jpg"}
            };
        }
    }
}
