﻿using Spanzuratoarea.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spanzuratoarea.Models
{
    class GameDetails : INotifyPropertyChanged
    {
        private string playersName;
        private string playersPath;


        public string PlayersName
        {
            get
            {
                return playersName;
            }
            set
            {
                playersName = value;
                OnPropertyChanged("PlayersName");
            }
        }

        public string PlayersPath
        {
            get
            {
                return playersPath;
            }
            set
            {
                playersPath = value;
                OnPropertyChanged("PlayersPath");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
