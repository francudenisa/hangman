﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spanzuratoarea.Models
{
    class Statistics : INotifyPropertyChanged
    {
        private string name;
        private string winnings, loss;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Winnings
        {
            get
            {
                return winnings;
            }
            set
            {
                winnings = value;
                OnPropertyChanged("Winnings");
            }
        }

        public string Loss
        {
            get
            {
                return loss;
            }
            set
            {
                loss = value;
                OnPropertyChanged("Loss");
            }
        }

        

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
