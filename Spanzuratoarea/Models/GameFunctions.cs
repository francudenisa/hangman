﻿using Spanzuratoarea.Pictures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spanzuratoarea.Models
{
    class GameFunctions
    {
        public string GetWordUnderlines(string word)
        {
            int numberOfLetters = word.Count();
            string wordForTextBox = "";
            for (int index = 0; index < numberOfLetters; index++)
                if (word[index] != ' ')
                    wordForTextBox += "_ ";
                else
                    wordForTextBox += "  ";

            return wordForTextBox;
        }

        public string VerifyLetter(string wordToGuess, char letter, string playedWord)
        {
            string wordForText = "";
            for (int index = 0; index < wordToGuess.Count(); index++)
            {
                if (wordToGuess[index] == letter)
                {
                    wordForText += letter;
                    wordForText += " ";
                }
                else
                    wordForText += "_ ";
            }

            string intermediate = "";
            for (int index = 0; index < playedWord.Count(); index++)
                if (wordForText[index] != playedWord[index])
                {
                    if (wordForText[index] != '_' && wordForText[index] != ' ')
                        intermediate += wordForText[index];
                    else intermediate += playedWord[index];
                }
                else intermediate += wordForText[index];
            playedWord = intermediate;

            return playedWord;
        }

        public bool CheckLeterExist(string wordToGuess, char letter)
        {
            for (int index = 0; index < wordToGuess.Count(); index++)
                if (wordToGuess[index] == letter)
                    return true;
            return false;
        }

        public bool VerifyWord(string word)
        {
            for (int index = 0; index < word.Count(); index++)
                if (word[index] == '_')
                    return false;
            return true;
        }

        public void IncreaseWinnings(int position)
        {
            UserViewModel statistics = new UserViewModel();
            string filePath = @"..\..\..\Files\Statistics.txt";
            File.WriteAllText(filePath, String.Empty);
            int index = 0;
            string numberOfWinnings = statistics.PlayersStatistics[position].Winnings;
            int numberWinnings = Int32.Parse(numberOfWinnings);
            numberWinnings++;
            string editedLine = statistics.PlayersStatistics[position].Name + " " + numberWinnings.ToString() + " " + statistics.PlayersStatistics[position].Loss;
            foreach (var line in statistics.PlayersStatistics)
            {
                string addToList = "";
                if (index == position)
                    addToList = editedLine;
                else addToList = line.Name + " " + line.Winnings + " " + line.Loss;
                List<string> addTotxt = new List<string>();
                addTotxt.Add(addToList);
                File.AppendAllLines(filePath, addTotxt);
                index++;
            }

        }

        public void IncreaseLoss(int position)
        {
            UserViewModel statistics = new UserViewModel();
            string filePath = @"..\..\..\Files\Statistics.txt";
            File.WriteAllText(filePath, String.Empty);
            int index = 0;
            string numberOfLoss = statistics.PlayersStatistics[position].Loss;
            int numberLoss = Int32.Parse(numberOfLoss);
            numberLoss++;
            string editedLine = statistics.PlayersStatistics[position].Name + " " + statistics.PlayersStatistics[position].Winnings + " " + numberLoss.ToString();
            foreach (var line in statistics.PlayersStatistics)
            {
                string addToList = "";

                if (index == position)
                    addToList = editedLine;
                else addToList = line.Name + " " + line.Winnings + " " + line.Loss;

                List<string> addTotxt = new List<string>();
                addTotxt.Add(addToList);
                File.AppendAllLines(filePath, addTotxt);
                index++;
            }

        }

        public void SaveGameDetails(string name, string category, int level, string wordToGuess, string playedWord,
            int numberOfMistakes, int timer, string pressedButtons)
        {
            string filePath = @"..\..\..\Files\SaveGame.txt";
            
            int fileLength = File.ReadLines(filePath).Count();
            List<string> addText = new List<string>();
            string addTotxt = name + "," + category + "," + level.ToString() + "," +
                wordToGuess + "," + numberOfMistakes.ToString() + "," + timer + "," + pressedButtons + "," + playedWord;
            bool existsAlready = false;

            for (int index=0;index<fileLength;index++)
            {
                string clickedLine = File.ReadLines(filePath).Skip(index).Take(1).First();
                string[] items = clickedLine.Split(',');
                string nume = items[0];
                if (nume == name)
                {
                    existsAlready = true;
                    addText.Add(addTotxt);
                }
                else addText.Add(clickedLine);
            }
            if (existsAlready == false)
                addText.Add(addTotxt);
            File.WriteAllText(filePath, String.Empty);
            File.AppendAllLines(filePath, addText);
        }

        public string OpenSavedGame(string name)
        {
            string filePath = @"..\..\..\Files\SaveGame.txt";
            int fileLength = File.ReadLines(filePath).Count();
            string clickedLine = "";
            bool foundGame = false;
            for (int index = 0; index < fileLength; index++)
            {
                clickedLine = File.ReadLines(filePath).Skip(index).Take(1).First();
                string[] items = clickedLine.Split(',');
                string nume = items[0];
                if (nume == name)
                {
                    foundGame = true;
                    break;
                }
            }
            if (foundGame == true)
                return clickedLine;
            else return "";
        }

        public void DeleteUser(string name)
        {
            string filePathPlayers = @"..\..\..\Files\Players.txt";
            string filePathStatistics = @"..\..\..\Files\Statistics.txt";
            string filePathSaveGame = @"..\..\..\Files\SaveGame.txt";
            int fileLength = 0;
            List<string> addToTxt = new List<string>();

            //delete from players
            fileLength = File.ReadLines(filePathPlayers).Count();
            for(int index=0;index<fileLength;index++)
            {
                string line = File.ReadLines(filePathPlayers).Skip(index).Take(1).First();
                string[] items = line.Split(' ');
                if (items[0] != name)
                    addToTxt.Add(line);
            }

            File.WriteAllText(filePathPlayers, String.Empty);
            File.AppendAllLines(filePathPlayers, addToTxt);

            //delete from statistics
            addToTxt = new List<string>();
            fileLength = File.ReadLines(filePathStatistics).Count();
            for (int index = 0; index < fileLength; index++)
            {
                string line = File.ReadLines(filePathStatistics).Skip(index).Take(1).First();
                string[] items = line.Split(' ');
                if (items[0] != name)
                    addToTxt.Add(line);
            }

            File.WriteAllText(filePathStatistics, String.Empty);
            File.AppendAllLines(filePathStatistics, addToTxt);

            //delete from saved games
            addToTxt = new List<string>();
            fileLength = File.ReadLines(filePathSaveGame).Count();
            for (int index = 0; index < fileLength; index++)
            {
                string line = File.ReadLines(filePathSaveGame).Skip(index).Take(1).First();
                string[] items = line.Split(',');
                if (items[0] != name)
                    addToTxt.Add(line);
            }

            File.WriteAllText(filePathSaveGame, String.Empty);
            File.AppendAllLines(filePathSaveGame, addToTxt);
        }
        
    }

}
