﻿using Spanzuratoarea.Models;
using Spanzuratoarea.Pictures;
using Spanzuratoarea.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Brushes = System.Windows.Media.Brushes;

namespace Spanzuratoarea.Views
{
    /// <summary>
    /// Interaction logic for NewUser.xaml
    /// </summary>
    public partial class NewUser : Window
    {
        PicturePathsViewModel player = new PicturePathsViewModel();
        bool pictureSelected = false;
        string selectedPicture = "";
        string textBoxName = "";
        UserViewModel newPlayer = new UserViewModel();

        public NewUser()
        {
            InitializeComponent();
        }

        private void AddPlayer(object sender, RoutedEventArgs e)
        {
            textBoxName = NewPlayer.Text.ToString();
            if (textBoxName == "" || selectedPicture == "")
            {
                Warning objWarning = new Warning();
                objWarning.Show();
            }
            else
            {
                newPlayer.AddPlayer(textBoxName, selectedPicture);
                MainWindow objMainWindow = new MainWindow();
                objMainWindow.Show();
                Close();
            }
        }

        private void PictureSelected(object sender, RoutedEventArgs e)
        {
            int x = Pictures.SelectedIndex;
            x++;
            selectedPicture = "../../Pictures/pic" + x.ToString() + ".jpg";
        }


    }
}
