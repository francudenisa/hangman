﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Spanzuratoarea.Models;
using Spanzuratoarea.Views;
using Spanzuratoarea.ViewModels;
using Spanzuratoarea.Pictures;

namespace Spanzuratoarea
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string name, path;
        public MainWindow()
        {
            InitializeComponent();
        }


        private void Play(object sender, RoutedEventArgs e)
        {
            Game objAboutWindow = new Game();
            objAboutWindow.Show();
            Close();
        }
        
        private void NewUser(object sender, RoutedEventArgs e)
        {
            NewUser objNewUser = new NewUser();
            objNewUser.Show();
            Close();
        }

        private void ClickName(object sender, MouseButtonEventArgs e)
        {
            ButtonPlay.IsEnabled = true;
            ButtonDeleteUser.IsEnabled = true;
            int index = NamesList.SelectedIndex;

            string filePath = @"..\..\..\Files\Players.txt";
            string clickedLine = File.ReadLines(filePath).Skip(index).Take(1).First(); //here we take the data from txt of the selected user
            string[] items = clickedLine.Split(' ');
            name = items[0];
            path = items[1];
            string filePathGame = @"..\..\..\Files\GameDetails.txt";
            string addToTxt = '\n' + name + " " + path;
            List<string> da = new List<string>();
            da.Add(addToTxt);
            File.WriteAllText(filePathGame, String.Empty);
            File.AppendAllLines(filePathGame, da);

        }

        private void DeleteUser(object sender, RoutedEventArgs e)
        {
            GameFunctions functions = new GameFunctions();
            functions.DeleteUser(name);
            Confirm objConfirmWindow = new Confirm();
            objConfirmWindow.Show();
            Close();
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

    }
}
