﻿using Spanzuratoarea.Models;
using Spanzuratoarea.Pictures;
using Spanzuratoarea.ViewModels;
using Spanzuratoarea.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Spanzuratoarea
{
    /// <summary>
    /// Interaction logic for Game.xaml
    /// </summary>
    public partial class Game : Window
    {
        GameDetailsViewModel game = new GameDetailsViewModel();
        GameFunctions functions = new GameFunctions();
        DispatcherTimer dispatcherTimer = new DispatcherTimer();
        DateTime deadline;

        string filePath = @"..\..\..\Files\Players.txt";
        string wordToGuess = "";
        string playedWord = "";
        int numberOfMistakes = 0;
        int level = 1;
        int delay = 30;
        int secondsRemaining;
        int playersposition = -1;

        public Game()
        { 
            InitializeComponent();
            PlayGame();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            int index = 0;
            StreamReader reader = new StreamReader(filePath);
            string line = "";
            while ((line = reader.ReadLine()) != null)
            {
                string[] items = line.Split(' ');
                if (items[0] == game.gameDetails[0].PlayersName)
                    playersposition = index;
                index++;
            }
            
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            secondsRemaining = (deadline - DateTime.Now).Seconds;
            if (secondsRemaining == 0)
            {
                dispatcherTimer.Stop();
                dispatcherTimer.IsEnabled = false;
                functions.IncreaseLoss(playersposition);
                MessageBox.Show("Time has expired!");
                Statistics.IsEnabled = true;
                RetryButton.Visibility = Visibility.Visible;
                delay = 30;
                foreach (Button button in LettersButtons.Children.OfType<Button>())
                {
                    button.IsEnabled = false;
                }
            }
            else
            {
                TimerLabel.Content = secondsRemaining.ToString();
            }
        }

        private void About(object sender, RoutedEventArgs e)
        {
            About objAboutWindow = new About();
            objAboutWindow.Show();
        }

        private void FileClick(object sender, RoutedEventArgs e)
        {
            MenuItem itemChecked = (MenuItem)sender;
            
            string checkedElement = itemChecked.Header.ToString();

            if(checkedElement == "Exit")
            {
                dispatcherTimer.Stop();
                MainWindow objMainWindow = new MainWindow();
                objMainWindow.Show();
                Close();
            }

            if (checkedElement == "New Game")
            {
                Game objGameWindow = new Game();
                objGameWindow.Show();
                Close();
            }

            if(checkedElement == "Statistics")
            {
                //dispatcherTimer.Stop();
                
                Views.Statistics objStatisticsWindow = new Views.Statistics();
                objStatisticsWindow.Show();
            }

            if(checkedElement == "Open Game")
            {
                Wrong1.Text = ""; Wrong2.Text = ""; Wrong3.Text = ""; Wrong4.Text = ""; Wrong5.Text = ""; Wrong6.Text = "";
                string date = functions.OpenSavedGame(PlayerName.Text.ToString());
                if (date != "")
                {
                    string[] items = date.Split(',');
                    Category.Text = items[1];
                    level = Int32.Parse(items[2]);
                    Level.Text = "Level: " + items[2];
                    wordToGuess = items[3];
                    numberOfMistakes = Int32.Parse(items[4]);
                    CheckNumberOfMistakes(numberOfMistakes);
                    delay = Int32.Parse(items[5]);
                    deadline = DateTime.Now.AddSeconds(delay);
                    dispatcherTimer.Start();
                    string letters = items[6];
                    int letterindex = 0;
                    foreach (Button button in LettersButtons.Children.OfType<Button>())
                    {
                        if (letters[letterindex] == '1')
                            button.IsEnabled = false;
                        else button.IsEnabled = true;
                        letterindex++;
                    }
                    playedWord = items[7];
                    WordTextBox.Text = playedWord;
                }
                else PlayGame();
            }

            if(checkedElement == "Save Game")
            {
                string pressedButtons = "";
                foreach (Button button in LettersButtons.Children.OfType<Button>())
                {
                    if (button.IsEnabled == false)
                        pressedButtons += "1";
                    else pressedButtons += "0";
                }
                functions.SaveGameDetails(PlayerName.Text.ToString(), Category.Text.ToString(),
                level, wordToGuess, playedWord, numberOfMistakes, secondsRemaining,pressedButtons);
            }
        }

        private void CategoryClick(object sender, RoutedEventArgs e)
        {

            MenuItem itemChecked = (MenuItem)sender;
            MenuItem itemParent = (MenuItem)itemChecked.Parent;
            
            string checkedElement = itemChecked.Header.ToString();
            
            foreach (MenuItem item in itemParent.Items)
            {
                if (item == itemChecked) continue;

                item.IsChecked = false;
            }
            Category.Text = checkedElement;
            level = 1;
            delay = 30;
            PlayGame();
        }

        private void PlayGame()
        {
            deadline = DateTime.Now.AddSeconds(delay);
            dispatcherTimer.Start();
            Statistics.IsEnabled = false;
            RetryButton.Visibility = Visibility.Hidden;
            wordToGuess = game.GetRandomWord(Category.Text.ToString());
            playedWord = functions.GetWordUnderlines(wordToGuess);
            WordTextBox.Text = playedWord;
            HangmanPicture.Source = new BitmapImage(new Uri(@"../../Pictures/Hangman/hang1.jpg", UriKind.Relative));

            foreach (Button button in LettersButtons.Children.OfType<Button>())
            {
                button.IsEnabled = true;
            }

            string levelforTextBox = "Level: " + level.ToString();

            Level.Text = levelforTextBox;
            Wrong1.Text = ""; Wrong2.Text = ""; Wrong3.Text = ""; Wrong4.Text = ""; Wrong5.Text = ""; Wrong6.Text = "";
            numberOfMistakes = 0;
            NextButton.Visibility = Visibility.Hidden;
        }
        
        private void Retry(object sender, RoutedEventArgs e)
        {
            level = 1;
            PlayGame();

        }

        private void Next(object sender, RoutedEventArgs e)
        {
            PlayGame();
            if (level == 6)
            {
                Game objGameWindow = new Game();
                objGameWindow.Show();
                Close();
            }
        }

        private void CheckNumberOfMistakes(int numberOfMistakes)
        {
            if (numberOfMistakes >= 1)
            {
                Wrong1.Text = "X";
                HangmanPicture.Source = new BitmapImage(new Uri(@"../../Pictures/Hangman/hang2.jpg", UriKind.Relative));
            }
            if (numberOfMistakes >= 2)
            {
                Wrong2.Text = "X";
                HangmanPicture.Source = new BitmapImage(new Uri(@"../../Pictures/Hangman/hang3.jpg", UriKind.Relative));
            }
            if (numberOfMistakes >= 3)
            {
                Wrong3.Text = "X";
                HangmanPicture.Source = new BitmapImage(new Uri(@"../../Pictures/Hangman/hang4.jpg", UriKind.Relative));
            }
            if (numberOfMistakes >= 4)
            {
                Wrong4.Text = "X";
                HangmanPicture.Source = new BitmapImage(new Uri(@"../../Pictures/Hangman/hang5.jpg", UriKind.Relative));
            }
            if (numberOfMistakes >= 5)
            {
                Wrong5.Text = "X";
                HangmanPicture.Source = new BitmapImage(new Uri(@"../../Pictures/Hangman/hang6.jpg", UriKind.Relative));
            }
            if (numberOfMistakes == 6)
            {
                foreach (Button button in LettersButtons.Children.OfType<Button>())
                {
                    button.IsEnabled = false;
                }
                Wrong6.Text = "X";
                HangmanPicture.Source = new BitmapImage(new Uri(@"../../Pictures/Hangman/hang7.jpg", UriKind.Relative));
                functions.IncreaseLoss(playersposition);
                RetryButton.Visibility = Visibility.Visible;
                dispatcherTimer.Stop();
                Statistics.IsEnabled = true;
            }
        }

        private void Letter_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            string keyword = btn.Content.ToString();
            char letter = keyword[0];           
            btn.IsEnabled = false;
            if (functions.VerifyWord(playedWord) == false)
            {
                if (functions.CheckLeterExist(wordToGuess, letter) == true)
                {
                    string word = functions.VerifyLetter(wordToGuess, letter, playedWord);
                    WordTextBox.Text = word;
                    playedWord = word;
                    if (functions.VerifyWord(playedWord) == true)
                    {
                        foreach (Button button in LettersButtons.Children.OfType<Button>())
                        {
                            button.IsEnabled = false;
                        }
                        level++;
                        NextButton.Visibility = Visibility.Visible;
                        dispatcherTimer.Stop();
                        Statistics.IsEnabled = true;
                        if (level == 6)
                        {
                            NextButton.Content = "Next game";
                            functions.IncreaseWinnings(playersposition);
                            
                        }
                    }
                }
                else
                {
                    numberOfMistakes++;
                    CheckNumberOfMistakes(numberOfMistakes);
                }
            }
            
        }
    }
}
